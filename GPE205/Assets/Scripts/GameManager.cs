﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    //Create a static instance variable and apply singleton logic in the Awake function
    public static GameManager instance;

    public TankData playerTank;
    public List<TankData> enemyTanks;

    void Awake()
    {
        
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(instance);
        }
        else
        {
            Destroy(instance.gameObject);
        }

    }

    public TankData GetClosestEnemy()
    {
        TankData closestTank = enemyTanks[0];

        if (enemyTanks.Count > 1)
        {
            for (int i = 1; i < enemyTanks.Count; i++)
            {
                if (Vector3.SqrMagnitude(enemyTanks[i].tf.position - playerTank.tf.position) <
                    Vector3.SqrMagnitude(closestTank.tf.position - playerTank.tf.position))
                {
                    closestTank = enemyTanks[i];
                }
            }
        }

        return closestTank;
    }

}
