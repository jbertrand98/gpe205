﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : MonoBehaviour
{

    public enum AIState
    {
        Wander,
        Patrol,
        Chase,
        Flee,
        Avoid
    }

    public TankData pawn;
    public TankData closestPlayer;
    public List<Transform> waypoints;

    public float closeEnough;
    public float avoidDistance;

    // Start is called before the first frame update
    void Start()
    {

        pawn = gameObject.GetComponent<TankData>();

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChangeState()
    {

    }

    public void Wander()
    {

    }

    public void Patrol()
    {

    }

    public void Chase()
    {

    }

    public void Flee()
    {

    }

    public void Avoid()
    {

    }


}
