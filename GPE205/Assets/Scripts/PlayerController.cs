﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    //Enum for control scheme
    public enum ControlScheme { WASD, Arrows };
    public ControlScheme controlScheme;

    public TankData pawn;
    private TankData closestTank;

    // Update is called once per frame
    void Update()
    {
        //Determine what control scheme is being used
        switch (controlScheme)
        {
            case ControlScheme.WASD:
                //Send user input to tank mover
                pawn.mover.Move(Input.GetAxis("WASDVertical"));
                pawn.mover.Rotate(Input.GetAxis("WASDHorizontal"));
                break;

            case ControlScheme.Arrows:
                //Send user input to tank mover
                pawn.mover.Move(Input.GetAxis("ArrowVertical"));
                pawn.mover.Rotate(Input.GetAxis("ArrowHorizontal"));
                break;
        }

        closestTank = GameManager.instance.GetClosestEnemy();
        if (pawn.senses.CanSee(closestTank.gameObject))
        {
            pawn.mover.RotateBarrelTowards(closestTank.tf.position);
        }
        else
        {
            pawn.mover.RotateBarrelTowards(pawn.tf.position + pawn.tf.forward);
        }

        //Check for fire button
        if (Input.GetKeyDown("space"))
        {
            //Send shoot command
            pawn.shooter.Shoot();
        }

    }
}
