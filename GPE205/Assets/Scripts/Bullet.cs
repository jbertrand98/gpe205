﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private Transform tf;
    private Rigidbody rb;
    private TankData shooter;
    private float killTimer;

    // Start is called before the first frame update
    void Start()
    {
        tf = gameObject.GetComponent<Transform>();
        rb = gameObject.GetComponent<Rigidbody>();
        rb.AddForce(tf.forward * shooter.bulletSpeed);
        killTimer = Time.time + shooter.bulletLifetime;
    }

    // Update is called once per frame
    void Update()
    {
        //Check if bullet has exceeded its lifetime
        if (Time.time > killTimer)
        {
            Destroy(this.gameObject);
        }
    }

    public void SetShooter(TankData newShooter)
    {
        shooter = newShooter;
    }

    public void OnCollisionEnter(Collision collision)
    {
        //Get the gameObject that was collided with
        GameObject otherObject = collision.gameObject;
        //Try to get a tankData component from otherObject
        TankData otherData = otherObject.GetComponent<TankData>();

        //If the other object has a tankData
        if (otherData != null)
        {
            //Do damage to the other tank
            otherData.damager.TakeDamage(shooter.bulletDamage, shooter);
        }

        //Destroy this bullet
        Destroy(this.gameObject);
    }

}
