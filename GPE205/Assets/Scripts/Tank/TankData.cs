﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TankDamager))]
[RequireComponent(typeof(TankMover))]
[RequireComponent(typeof(TankScorekeeper))]
[RequireComponent(typeof(TankSenses))]
[RequireComponent(typeof(TankShooter))]

public class TankData : MonoBehaviour
{

    public enum TankType { PlayerTank, AITank };

    [Header("Components")]
    public Transform tf;
    public Transform barrel;
    public TankDamager damager;
    public TankMover mover;
    public TankScorekeeper scorekeeper;
    public TankSenses senses;
    public TankShooter shooter;

    [Header("Tank Variables")]
    public TankType tankType;
    public float forwardSpeed;
    public float reverseSpeed;
    public float rotateSpeed;
    public float maxBarrelRotation;
    public float barrelRotateSpeed;
    public float fieldOfView;
    public float sightRange;
    public int health;
    public int pointsWorth;

    [Header("Bullet Variables")]
    public float bulletSpeed;
    public float bulletDelay;
    public float bulletLifetime;
    public int bulletDamage;

    // Start is called before the first frame update
    void Start()
    {

        tf = gameObject.GetComponent<Transform>();
        barrel = gameObject.transform.GetChild(0).GetChild(1);
        damager = gameObject.GetComponent<TankDamager>();
        mover = gameObject.GetComponent<TankMover>();
        scorekeeper = gameObject.GetComponent<TankScorekeeper>();
        senses = gameObject.GetComponent<TankSenses>();
        shooter = gameObject.GetComponent<TankShooter>();

        if (tankType == TankType.PlayerTank)
        {
            GameManager.instance.playerTank = this;
        }
        else if (tankType == TankType.AITank)
        {
            GameManager.instance.enemyTanks.Add(this);
        }

    }

}
