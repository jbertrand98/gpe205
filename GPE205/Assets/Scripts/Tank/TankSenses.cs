﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TankData))]

public class TankSenses : MonoBehaviour
{

    private TankData data;

    // Start is called before the first frame update
    void Start()
    {
        data = gameObject.GetComponent<TankData>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public bool CanSee(GameObject target)
    {
        Transform targetTransform = target.GetComponent<Transform>();
        Vector3 lookVector = targetTransform.position - data.tf.position;
        lookVector = Vector3.Normalize(lookVector);
        RaycastHit hit;

        if (Vector3.Angle(data.tf.forward, lookVector) <= data.fieldOfView)
        {
            if (Physics.Raycast(data.tf.position, lookVector, out hit, data.sightRange))
            {
                if (hit.collider.gameObject == target)
                {
                    Debug.Log("True");
                    return true;
                }
            }
        }

        return false;
    }

}
