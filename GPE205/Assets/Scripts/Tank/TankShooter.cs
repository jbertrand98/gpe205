﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TankData))]

public class TankShooter : MonoBehaviour
{
    //tankData and a copy of the bullet to be fired
    private TankData data;
    public GameObject bullet;


    //Place to store the new bullets that are instantiated
    private GameObject newBullet;
    private Bullet bulletData;

    //The time at which the last bullet was fired
    private float lastBullet;

    // Start is called before the first frame update
    void Start()
    {
        data = gameObject.GetComponent<TankData>();
        //Set lastBullet to an arbitrary negative value so the tank can shoot right away
        lastBullet = -100;
    }

    public void Shoot()
    {
        //Check to see if enough time has passed for a bullet to be shot
        if (Time.time > lastBullet + data.bulletDelay)
        {
            //Instantiate a new bullet
            newBullet = Instantiate(bullet, data.tf.position + data.barrel.forward, data.barrel.rotation);
            //Get the bullet component and set its shooter to this tank
            bulletData = newBullet.GetComponent<Bullet>();
            bulletData.SetShooter(data);
            //Update timer
            lastBullet = Time.time;
        }
    }

}
