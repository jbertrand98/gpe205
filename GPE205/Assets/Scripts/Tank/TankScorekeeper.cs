﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TankData))]

public class TankScorekeeper : MonoBehaviour
{

    private TankData data;
    private int score;

    // Start is called before the first frame update
    void Start()
    {
        data = gameObject.GetComponent<TankData>();
        score = 0;
    }
        
    public int GetScore()
    {
        return score;
    }

    public void AddPoints(int points)
    {
        score += points;
        Debug.Log(score);
    }

}
