﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TankData))]
[RequireComponent(typeof(CharacterController))]

public class TankMover : MonoBehaviour
{

    private TankData data;
    private CharacterController characterController;
    private float barrelAngle;
    private float speed;

    // Start is called before the first frame update
    void Start()
    {

        data = gameObject.GetComponent<TankData>();
        characterController = gameObject.GetComponent<CharacterController>();
    }

    public void Move(float direction)
    {
        //Set which move speed to use
        if (direction > 0)
        {
            speed = data.forwardSpeed;
        }
        else
        {
            speed = data.reverseSpeed;
        }
        //Move the tank
        characterController.SimpleMove(data.tf.forward * direction * speed);
    }

    public void Rotate(float direction)
    {
        //Rotate the tank
        data.tf.Rotate(data.tf.up * direction * data.rotateSpeed * Time.deltaTime);
    }

    public void RotateBarrelTowards(Vector3 target)
    {
        Quaternion currentRotation = data.barrel.rotation;

        Quaternion lookRotation = Quaternion.LookRotation(target - data.tf.position);

        data.barrel.rotation = Quaternion.RotateTowards(data.barrel.rotation, lookRotation, data.barrelRotateSpeed * Time.deltaTime);

        //Get the angle of the barrel
        barrelAngle = Vector3.Angle(data.tf.forward, data.barrel.forward);
        
        //if the rotation exceeds the rotation limit, undo it
        if (barrelAngle >= data.maxBarrelRotation)
        {
            data.barrel.rotation = currentRotation;
        }
    }

}
