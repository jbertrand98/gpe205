﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TankData))]

public class TankDamager : MonoBehaviour
{

    private TankData data;

    // Start is called before the first frame update
    void Start()
    {
        data = gameObject.GetComponent<TankData>();
    }
        
    public void TakeDamage(int damage, TankData hitBy)
    {
        //Take damage
        data.health -= damage;

        //Check if damage destroyed tank
        if (data.health <= 0)
        {
            GetDestroyed(hitBy);
        }
    }

    private void GetDestroyed(TankData hitBy)
    {
        //Add points to the score of the tank that destroyed this tank
        hitBy.scorekeeper.AddPoints(data.pointsWorth);

        if (GameManager.instance.enemyTanks.Contains(data))
        {
            GameManager.instance.enemyTanks.Remove(data);
        }

        //Destroy this tank
        Destroy(this.gameObject);
    }

}
